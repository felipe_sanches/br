'- Assembler para o Spectrum , ZX-Spectrum , INPUT Vol 1 Num 13 , Página 248
'- não testado
'- <Lisias Toledo> me@lisias.net

5000 DIM k$(110,4):DIM k(110):DIM m(110):LET h$="0123456789ABCDEF":LET b$="":LET q$="0123456789abcdef"
5010 DIM T$(110,24):DIM r(100):DIM z$(100,6):DIM z(100)
5020 DIM b(9):LET b(1)=1:FOR i=2 TO 9:LET b(i)=b(i-1)+b(i-1):NEXT i
5030 DIM r$(8,4,4):FOR j=1 TO 4:FOR i=1 TO 8:READ r$(i,j):NEXT I:NEXT J
5040 DATA "0","1","2","3","4","5","6","7","nz","z","nc","c","po","pe","p","m","0","8","16","24","32","40","48","56","hl","ix","iy","bc","de","hl","sp"," "
5050 DIM s$(8,2,4):DIM t(18):DIM u$(18,10):FOR j=1 TO 2:FOR i=1 TO 8/j:READ s$(i,j):NEXT i:NEXT j:FOR j=1 TO 18:READ t(j),u$(j):NEXT j
5060 DATA "b","c","d","e","h","l","(hl)","a","bc","de","hl","sp",235,"de",8,"af",227,"(sp)",60742,"0",60758,"1",60766,"2",233,"(hl)",56809,"(ix)",65001,"(iy)",10,"(bc)",26,"(de)",60767,"r",2,"(bc)",18,"(de)",60751,"r",249,"sp",60743,"i",60759,"i"
5070 DEF FN b(x,i)=INT(x/b(i+1))-INT(x/b(i+2))*2
5080 DEF FN x(x,i)=x-b(i+2)*FN b(x,i)+b(i+1)
5090 DEF FN j(x,i)=INT(x-b(i+1)*INT(x/b(i+1))
5100 DEF FN e(i$,j$)=(i$=j$(TO LEN(i$)))
5110 FOR i =1 TO 110:READ k$(i),k(i),m(i):IF NOT FN e("*",k$(i)) THEN NEXT i
5120 DATA "ld",10,10, "ld",26,10,"ld",60767,10,"ld",60759,10,"ld",2,138,"ld",18,138,"ld",60751,138,"ld",60743,138,"ld",64,6,"ld",50,202,"ld",58,74,"ld",249,139,"ld",34,195,"ld",42,67,"ld",60779,197,"ld",60771,199,"ld",97,165,"ld",64,54
5130 DATA "adc",136,50,"adc",60746,3,"add",128,50,"add",9,149,"and",160,48,"or",176,48,"xor",168,48,"nop",0,0,"sub",144,48,"sbc",152,50,"sbc",60738,3,"cp",184,48,"jp",130,45,"jp",233,9,"jp",56809,9,"jp",65001,9,"jp",131,49,"jr",96,45,"jr",88,41
5140 DATA "call",132,45,"call",141,41,"ret",201,0,"djnz",74,40,"dec",11,17,"dec",5,16,"inc",3,17,"inc",4,16,"push",197,17,"pop",193,17,"di",243,0,"ei",251,0,"halt",118,0,"ex",235,139,"ex",8,15,"ex",227,143,"exx",217,0
5150 DATA "rst",199,132,"rts",192,5,"bit",52032,20,"defb",-256,40,"ccf",63,0,"scf",55,0,"cpl",47,0,"cpd",60841,0,"cpdr",60857,0,"cpi",60833,0,"cpir",60849,0,"daa",39,0,"im",60742,8,"im",60758,8,"im",60766,8
5160 DATA "in",60736,130,"in",149,42,"ind",60848,0,"indr",60810,0,"ini",60840,0,"ldd",60840,0,"lddr",60856,0,"ldi",60832,0,"ldir",60848,0,"neg",60740,0,"otdr",60859,0,"otir",60851,0,"out",60737,2,"out",141,170,"outd",60843,0,"outi",60835,0
5170 DATA "res",52096,20,"reti",60749,0,"retn",60741,0,"rl",51984,64,"rla",23,0,"rlc",51968,16,"rlca",7,0,"rld",60783,0,"rr",51992,64,"rra",31,0,"rrc",51976,16,"rrca",15,0,"rrd",60775,0
5180 DATA "set",52160,20,"sla",52000,16,"sra",52008,16,"srl",52024,16,"defw",-256,41
5190 DATA "*",0,0:LET ii=i:LET k(110)=ii
5200 LET b=0:LET ba=PEEK(23635)+256*PEEK(23636)+4:LET n=1
5210 LET cc=1:IF PEEK(ba)<>234 THEN LET n=n-1:GOTO 5250
5220 LET cc=cc+1:LET ba=ba+1:IF PEEK(ba)=13 THEN LET ba=ba+5:LET n=n+1:GOTO 5210
5230 LET t$(n,cc)=CHR$(PEEK(ba))
5240 GOTO 5220
5250 FOR g=1 TO 100:LET r(g)=g-1:NEXT g:LET fh=100
5300 LET k0=0:LET k9=99:LET p0=0:LET vv=0
5310 LET k=k0:LET p=p0
5320 GOSUB 8000
5330 GOSUB 7000:LET o$=i$:IF o$(1)="*" THEN PRINT o$;:GOTO 5320
5340 IF o$="end" THEN PRINT '" endereco final ";p-1
5350 IF o$="end" THEN LET p0=p:GOTO 9999
5370 IF o$<>"org" THEN GOTO 5400
5380 GOSUB 7000:LET s=0:IF i$(1)="*" THEN LET s=p:LET i$=i$(2 TO)
5390 LET p=VAL(i$)+s:PRINT '" org ";p;:GOTO 5320
5400 IF p=0 THEN PRINT "(falta org)":LET p=50000
5410 LET p$=o$+"!":FOR i=1+18*(o$<>"ld") TO 110:IF o$<=k$(i) AND p$>k$(i) THEN GOTO 5500
5420 NEXT i: PRINT o$
5430 IF i$(1)="." THEN LET i$=i$(2 TO)
5440 GOSUB 9000:LET gg=r(g)
5450 IF gg<=100 THEN LET s=SGN(z(gg)):LET b=INT(ABS(z(gg)/65536)):LET r=ABS(z(gg)-b*65536):LET q=PEEK(r)+256*PEEK(r+1):POKE r,FN j(p*s+q,8):PRINT " POKE ";r;" com ";FN j(p*s+q,8):IF b THEN POKE(r+1),FN j((p*s+q)/256,8):PRINT " POKE ";r+1;" com ";FN j((p*s+q)/256,8)
5460 IF gg<=100 THEN LET gh=r(gg):LET r(gg)=fh:LET fh=gg:LET gg=gh:GOTO 5450
5470 IF i$="" THEN LET r(g)=p+100:GOTO 5330
5480 PRINT " (Linha nao foi reconhecida)"
5490 GOTO 5420
5500 LET z=0:LET r=0:LET e=0:PRINT "    ";o$;
5510 LET op=k(i):IF m(i)=0 THEN GOTO 6090
5520 GOSUB 7000:LET a$=i$:PRINT " ";a$;
5530 LET m=m(i):LET op=k(i):LET b=FN b(m,0):LET b7=b+2*FN b(m,7)+1:LET z=0:IF FN j(m,3)<2 THEN LET c$=a$:GOTO 5720
5540 FOR j=1 TO LEN(a$):IF a$(j)="," THEN GOTO 5580
5550 NEXT j:IF o$="rst" OR o$="rts" THEN GOTO 5580
5560 IF FN e(o$,k$(i+1)) THEN LET i=i+1:GOTO 5530
5570 PRINT " (deve haver dois operandos)":GOTO 5320
5580 LET b$=a$(TO j-1):LET c$=a$(j+1TO)
5590 IF FN b(m,2) THEN GOTO 5650
5600 IF FN b(m,7) THEN LET d$=c$:LET c$=b$:LET b$=d$
5610 IF b$="ahl"(b+1 TO b+b+1) THEN GOTO 5720
5620 IF b$="(c)" AND (o$="in" OR o$="out") THEN GOTO 5720
5630 IF (FN e(o$,k$(i+1))) AND (FN j(m(i+1),3)>=2) THEN LET i=i+1: GOTO 5530
5640 PRINT "(primeiro operando deve ser a ou hl)":GOTO 5320
5650 IF FN b(m,1) THEN GOTO 5690
5660 LET e$=(b$+"   ")(TO 4):FOR j=1 TO 8: IF e$=r$(j,b7) THEN LET op=op+8*(j-1)*(b7<4)+16*(j-6)*(b7=4)*(j>3):LET z=(j-1)*(b7=4)*(j<=3):GOTO 5710
5670 NEXT j:IF p$>k$(i+1) AND (FN j(m(i+1),3)>=2) THEN LET i=i+1:GOTO 5530
5680 PRINT "(primeiro operando deve ser bit ou sinalizador)":GOTO 5320
5690 IF FN b(m,7) THEN LET d$=c$:LET c$=b$:LET b$=d$:GOTO 5660
5700 LET x=8:GOSUB 5750:IF e THEN GOTO 5730
5710 IF c$="" THEN GOTO 6090
5720 LET x=1+15*b+7*(op<=6 AND op>=4 OR b$="(c)"):LET b$=c$:GOSUB 5730 IF e=2 OR p$>k$(i+1) AND FN j(m(i+1),3)=FN j(FN x(m,0),3) THEN LET e=0:LET i=i+1:GOTO 5530
5740 GOTO 5320
5750 LET r=0:IF FN b(m,4) AND FN e("("(TO NOT b),b$) THEN LET z2=FN e("ix",b$(2-b TO)+" ")+2*FN e("iy",b$(2-b TO)+" "): IF z2 THEN LET z=z2:LET e$=b$(TO LEN(b$)-NOT b):LET b$="(hl)"(1+b TO 4-b):LET f$="0"+e$(4-b TO)
5760 IF FN b(m,r) THEN GOTO 5790
5770 LET e$=(b$+"   ")(TO 4):FOR j=1 TO 8/(b+1):IF e$=s$(j,b+1) THEN LET op=op+(j-1)*x:RETURN
5780 GOTO 5810
5790 LET j2=9+9*(o$="ld"):FOR J=J2-8 to J2:IF k(i)<>t(j) THEN GOTO 5810
5800 IF FN e(b$,u$(j)) THEN RETURN
5810 NEXT j:IF b$="af" THEN IF FN e("p",o$) THEN LET op=op+48:RETURN
5820 IF FB b(m,6) AND FN e("(",b$) THEN LET b$=b$(2 TO LEN(b$)-1):GOTO 5860
5830 IF FN b(m,5) THEN LET op=FN x(op+6*NOT(b),6):GOTO 5860
5840 IF p$>k$(i+1) THEN LET e=2:RETURN
5850 PRINT "(operando incompativel)":LET e=1:RETURN
5860 LET r=65536
5870 LET s=1
5880 IF b$="" THEN GOTO 6080
5890 LET x$=b$(1):LET d$=b$(2 TO): IF x$="*" THEN LET r=r+p*s:LET b$=d$:GOTO 5870
5900 IF x$="+" THEN LET b$=d$:GOTO 5880
5910 IF x$="-" THEN LET b$=d$:LET s=-s:GOTO 5880
5920 IF x$="'" THEN LET r=r+CODE(d$)*s:LET b$=d$(2 TO):GOTO 5870
5930 LET q=0:IF x$<>"%" OR d$<"0" OR d$>"2" THEN GOTO 5960
5940 IF d$>="0" AND d$<"2" THEN LET q=q*2+CODE(d$)-48:LET d$=d$(2 TO):GOTO 5940
5950 LET r=r+q*s:LET b$=d$:GOTO 5870
5960 IF x$<>"$" OR d$<"0" OR d$>="g" THEN GOTO 6000
5970 LET x$=CHR$(CODE(d$)):FOR g=0 TO 15:IF x$<>h$(g+1) AND x$<>g$(g+1) THEN GOTO 5990
5980 LET q=q*16+g:LET d$=d$(2 TO):GOTO 5970
5990 NEXT g:LET r=r+q*s:LET b$=d$:GOTO 5870
6000 IF x$<"a" OR x$>"z" THEN GOTO 6040
6010 LET i$=b$:GOSUB 9000:IF i$<>"" THEN GOSUB 9400
6020 IF r(g)<>23000 AND r(g)>100 THEN LET r=r+(r(g)-100)*s:LET b$=i$:GOTO 5870
6030 IF r(g)=23000 OR r(g)<=100 THEN LET gh=r(fh):LET r(fh)=r(g):LET r(g)=fh:LET fh=gh:LET z(r(g))=(p+SGN(op)+(ABS(op)>255)+2*(z>0)+65536*((b OR FN b(m6)) AND o$<>"jr"))*s:LET b$=i$:GOTO 5870
6040 IF X$<"0" OR x$>"9" THEN LET r=0:GOTO 6070
6050 IF b$>="0" AND b$<":" THEN LET q=q*10+CODE(b$)-48:LET b$=b$(2 TO):GOTO 6050
6060 LET r=r+s*q:GOTO 5870
6070 PRINT "(enderecamento incorreto)"
6080 LET r=r-(p+2)*(o$="djnz" OR o$="jr"):RETURN
6090 PRINT TAB(16);:LET by=p/256:GOSUB 6190:LET by=p:GOSUB 6190:GOSUB 6160
6100 IF z THEN LET by=189+z*32:GOSUB 6180:GOSUB 6160
6110 IF op>=0 THEN LET by=op/256:GOSUB 6170:GOSUB 6150:LET by=op:GOSUB 6180
6120 IF r=0 THEN GOTO 5320
6130 GOSUB 6160:LET by=r:GOSUB 6180:IF (b OR FN b(m,6)) AND o$<>"jr" THEN LET by=r/256:GOSUB 6180
6140 GOTO 5320
6150 IF z AND INT(by) AND NOT b THEN GOSUB 6260:LET by=VAL(f$):GOSUB 6180:LET z=0
6160 PRINT " ";: RETURN
6170 IF INT(by)<=0 THEN RETURN
6180 LET by=FN j(by,8):POKE p,by:LET p=p+1
6190 LET by=FN j(by,8):PRINT h$(1+INT(by/16));h$(FN j(by,4)+1);
6200 RETURN
7000 IF k>n THEN LET i$="end":RETURN
7010 LET k1=k9+1:IF k9>=LEN(t$(k)) THEN LET i$="/faltando/":RETURN
7020 LET k9=k1:IF t$(k,k1)=" " THEN GOTO 7010
7030 IF k9>LEN(t$(k)) THEN LET i$=t$(k)(k1 TO):RETURN
7040 IF t$(k,k9)<>" " THEN LET k9=k9+1:GOTO 7030
7050 LET i$=t$(k)(k1 TO k9-1):RETURN
8000 IF k>0 THEN IF t$(k)(k9 TO)>t$(99) THEN PRINT t$(k)(k9 TO);
8010 POKE 23692,0:LET k=k+1:LET k9=0
8020 PRINT:RETURN
9000 LET x$=""
9010 IF i$<"a" OR i$>"z" THEN GOTO 9030
9020 LET x$=x$+i$(1):LET i$=i$(2 TO):GOTO 9010
9030 IF i$<>"" THEN RETURN
9400 FOR g=1 TO vv:IF FN e(x$,z$(g)) THEN RETURN
9410 NEXT g:LET vv=vv+1:LET z$(vv)=x$:LET g=vv:LET r(g)=23000
9420 RETURN

