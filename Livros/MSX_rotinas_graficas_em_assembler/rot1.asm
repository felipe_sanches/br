//Transcrição literal da página 20

== ROT1 ==

Função:
    Deslocamento de caracteres para cima.

Endereco inicial:      56000 = 0xDAC0
Endereco final:        56088 = 0xDB18
Código de verificação: 10572 = 0x294C

Descrição:
    O endereço 62000 contém a coordenada vertical correspondente
ao canto superior esquerdo da zona no interior da qual os
caracteres são deslocados. No modo vídeo 1, este número está
compreendido entre 1 e 32.
    O endereço 62001 contém a coordenada horizontal correspondente
ao canto superior esquerdo desta mesma zona. No modo vídeo 1, este
número está compreendido entre 1 e 24.
    O endereço 62002 contém um número compreendido entre 1 e 24
que representa a altura da zona.
    O endereço 62003 contém um número compreendido entre 1 e 32
que representa a largura da zona.

Utilização:
    No modo 0 e no modo 1.

 21 00 00 01 20 00 3a af
 fc 3d ca d0 da 01 28 00
 3a 31 f2 3d ca db da 09
 c3 d3 da 11 00 00 3a 30
 f2 3d 5f 19 ed 5b 22 f9
 19 e5 d1 3a 33 f2 f5 cd
 4a 00 32 40 f2 3a 32 f2
 3d f5 09 cd 4a 00 ed 42
 cd 4d 00 09 f1 3d c2 f9
 da 3a 40 f2 cd 4d 00 13
 d5 e1 f1 3d c2 ee da c9
 01


// Disassembly do bloco de bytes.
// Por Felipe C. da S. Sanches <juca@members.fsf.org>
// Esta tradução está lançada ao domínio público.

  DAC0: 21 00 00     LD HL, $0000
  DAC3: 01 20 00     LD BC, $0020
  DAC6: 3a af fc     LD A, ($FCAF)
  DAC9: 3d           DEC A
  DACA: ca d0 da     JP Z, $DAD0
  DACD: 01 28 00     LD BC, $0028
> DAD0: 3a 31 f2     LD A, ($F231)
> DAD3: 3d           DEC A
  DAD4: ca db da     JP Z, $DADB
  DAD7: 09           ADD HL, BC
  DAD8: c3 d3 da     JP $DAD3
> DADB: 11 00 00     LD DE, 0000
  DADE: 3a 30 f2     LD A, ($F230)
  DAE1: 3d           DEC A
  DAE2: 5f           LD E, A
  DAE3: 19           ADD HL, DE
  DAE4: ed 5b 22 f9  LD DE, ($F922)
  DAE8: 19           ADD HL, DE
  DAE9: e5           PUSH HL
  DAEA: d1           POP DE
  DAEB: 3a 33 f2     LD A, ($F233)
> DAEE: f5           PUSH AF
  DAEF: cd 4a 00     CALL $004A
  DAF2: 32 40 f2     LD ($F240), A
  DAF5: 3a 32 f2     LD A, ($F232)
  DAF6: 3d           DEC A
  DAF7: f5           PUSH AF
  DAF8: 09           ADD HL, BC
> DAF9: cd 4a 00     CALL $004A
  DAFC: ed 42        SBC HL,BC
  DAFE: cd 4d 00     CALL $004D
  DB01: 09           ADD HL, BC
  DB02: f1           POP AF
  DB03: 3d           DEC A
  DB04: c2 f9 da     JP NZ, $DAF9
  DB07: 3a 40 f2     LD A, ($F240)
  DB0A: cd 4d 00     CALL $004D
  DB0D: 13           INC DE
  DB0E: d5           PUSH DE
  DB0F: e1           POP HL
  DB10: f1           POP AF
  DB11: 3d           DEC A
  DB12: c2 ee da     JP NZ, $DAEE
  DB15: c9           RET
  DB16: 01           DB $01


// Interpretação do significado do código:
// Por Felipe C. da S. Sanches <juca@members.fsf.org>
// Esta interpretação está lançada ao domínio público.

  READ_VRAM:         EQU $004A # Read from Video RAM
                               # (in: HL=address out: A=value)

  WRITE_VRAM:        EQU $004D # Write to Video RAM
                               # (in: HL=address A=value)

  Y1:                EQU $F230 
  X1:                EQU $F231
  ALTURA:            EQU $F232
  LARGURA:           EQU $F233
  CHAR_VALUE:        EQU $F240
  NAME_BASE:         EQU $F922 #Base address of current pattern name table
  SCREEN_MODE:       EQU $FCAF

  SCREEN_WIDTH_MODE_0    EQU 40
  SCREEN_WIDTH_MODE_1    EQU 32

  DAC0: 21 00 00              LD HL, $0000
  DAC3: 01 20 00              LD BC, SCREEN_WIDTH_MODE_1
  DAC6: 3a af fc              LD A, (SCREEN_MODE)
  DAC9: 3d                    DEC A
  DACA: ca d0 da              JP Z, $IS_MODE_1
  DACD: 01 28 00              LD BC, SCREEN_WIDTH_MODE_0
> DAD0: 3a 31 f2  IS_MODE_1:  LD A, (X1)   \
> DAD3: 3d        MUL_SCR_W:  DEC A        |
  DAD4: ca db da              JP Z, DONE   | HL += (SCREEN_WIDTH * X1)
  DAD7: 09                    ADD HL, BC   |
  DAD8: c3 d3 da              JP MUL_SCR_W /
> DADB: 11 00 00  DONE:       LD DE, $0000     \
  DADE: 3a 30 f2              LD A, (Y1)       |
  DAE1: 3d                    DEC A            | HL += (Y1-1)
  DAE2: 5f                    LD E, A          |
  DAE3: 19                    ADD HL, DE       /
  DAE4: ed 5b 22 f9           LD DE, (NAME_BASE)  \  HL += NAME_BASE
  DAE8: 19                    ADD HL, DE          /
  DAE9: e5                    PUSH HL           \  DE = HL 
  DAEA: d1                    POP DE            /
  DAEB: 3a 33 f2              LD A, (LARGURA)
> DAEE: f5                    PUSH AF          #Guarda LARGURA na pilha
  DAEF: cd 4a 00              CALL READ_VRAM
  DAF2: 32 40 f2              LD (CHAR_VALUE), A
  DAF5: 3a 32 f2              LD A, (ALTURA)
  DAF6:(3d)                   DEC A
  DAF7: f5                    PUSH AF         #Guarda ALTURA-1 na pilha
  DAF8: 09                    ADD HL, BC
> DAF9: cd 4a 00              CALL READ_VRAM
  DAFC: ed 42                 SBC HL,BC         # HL -= SCREEN_WIDTH
  DAFE: cd 4d 00              CALL WRITE_VRAM
  DB01: 09                    ADD HL, BC
  DB02: f1                    POP AF          #Recupera ALTURA-1 da pilha
  DB03: 3d                    DEC A
  DB04: c2 f9 da              JP NZ, $DAF9
  DB07: 3a 40 f2              LD A, (CHAR_VALUE)
  DB0A: cd 4d 00              CALL WRITE_VRAM
  DB0D: 13                    INC DE
  DB0E: d5                    PUSH DE
  DB0F: e1                    POP HL
  DB10: f1                    POP AF
  DB11: 3d                    DEC A
  DB12: c2 ee da              JP NZ, $DAEE
  DB15: c9                    RET
  DB16: 01                    DB 1  #Esse byte parece ser inutil...

