'- Caverna Maldita , ZX-81 , Microcomputador Jogos , Página 91
'- status não informado pelo Digitador
'- Digitador desconhecido

10 REM                                   CAVERNA MALDITA             PARA LINHA SINCLAIR
20 SLOW
30 GOSUB 1550
40 CLS
50 LET OURO=0
60 LET P=0
70 LET D$=""
80 LET F=INT (RND*2000)+100
90 REM   LISTA COMANDOS
100 PRINT "COMANDOS-->",,"""C""PARA CHECAR",,"""L""PARA LUTAR",,"""R""PARA CORRER",,"""S""PARA ACEITAR UM PRESENTE","""N""PARA NAO ACEITAR UM PRESENTE"
110 SLOW
120 PRINT ,,"PRESSIONE QUALQUER TECLA"
130 IF INKEY$="" THEN GOTO 130
140 CLS
150 IF F>0 THEN GOTO 190
160 PRINT ,,"VOCE MORREU"
170 GOTO 1370
180 REM   CONTROLE DA DIRECAO
190 IF P<0 THEN LET P=0
200 IF P=0 THEN PRINT AT 0,0;"VOCE ESTA NA ENTRADA DA ","CAVERNA MALDITA"
210 IF P<>0 AND D$<>"F" THEN PRINT AT 3,0;"VOCE ESTA INDO NA DIRECAO ";D$;"."
220 PRINT AT 4,0;"ENTRE A DIRECAO N,S,L,O "
230 PRINT AT 5,0;"TESOURO=";OURO;"  ";AT 6,0;" PONTOS=";F;"  ";AT 7,0;"MOVIMENTOS=";P;"  "
240 REM   LE TECLA
250 LET D$=INKEY$
260 IF D$<>"N" AND D$<>"S" AND D$<>"O" AND D$<>"L" OR D$="" THEN GOTO 250
270 REM   GERA ENCONTRO
280 LET A=INT (RND*5)
290 IF A=0 THEN GOTO 390
300 IF A=1 THEN GOTO 820
310 IF A=2 THEN GOTO 980
320 IF P<0 THEN LET P=0
330 IF D$<>"S" THEN LET P=P+1
340 IF D$="S" THEN LET P=P-1
350 LET F=F-10
360 IF A<2 THEN CLS
370 GOTO 150
380 REM   ENCONTRA OS MONSTROS
390 CLS
400 PRINT "VOCE VIU UM MONSTRO"
410 PRINT "APERTE""R"" PARA CORRER OU ""L"" PARA LUTAR"
420 LET R$=INKEY$
430 IF INKEY$="" THEN GOTO 420
440 IF R$="R" THEN GOTO 320
450 PRINT ,,"VOCE ENCONTROU UM ";
460 LET M=INT (RND*3)+1
470 IF M=1 THEN LET N$="DRAGAO"
480 IF M=2 THEN LET N$="BRUXO"
490 IF M=3 THEN LET N$="GIGANTE"
500 PRINT N$
510 IF F<50 THEN LET A=LEN N$*2
520 IF F>=50 THEN LET A=INT (RND*F)+5
530 PRINT AT 10,0;"SEUS PONTOS-->";F;"  ";AT 11,0;"PONTOS DO "; N$;"-->";A;"  "
540 IF F<=0 THEN GOTO 1500
550 IF A<=0 THEN GOTO 1400
560 LET A=A-INT (RND*(F/2))
570 IF N$="BRUXO" THEN LET F=F-INT (RND*(A/2))
580 IF N$="GIGANTE" THEN LET F=F-INT (RND*(A/4))
590 IF N$="DRAGAO" THEN LET F=F-INT (RND*(A/2))
600 SLOW
610 GOTO 530
620 CLS
630 PRINT ,,"VOCE VE UM VELHO HOMEM QUE","LHE OFERECE UM PRESENTE","VOCE ACEITA?(S/N)"
640 LET G$=INKEY$
650 IF G$<>"S" AND G$<>"N" THEN GOTO 640
660 IF G$="N" THEN RETURN
670 PRINT ,"VOCE GANHOU UM";
680 LET G=INT (RND*3)+1
690 IF G=1 THEN LET X$="A ESPADA DE GRANDE PODER"
700 IF G=2 THEN LET X$=" ESCUDO "
710 IF G=3 THEN LET X$=" ELMO "
720 PRINT X$
730 LET D=INT (RND*6)+1
740 IF D<=2 THEN GOTO 770
750 PRINT ,,"QUE PENA,A ARMA ESTA AMALDICOADA"
760 PRINT ,,"VOCE PERDEU PONTOS"
770 IF F<=0 THEN GOTO 160
780 LET OURO=OURO+INT (RND*1000)+25
790 PRINT ,,"PONTOS=",F
800 RETURN
810 REM   ENCONTRA BAU
820 CLS
830 PRINT ,,"NA SUA FRENTE HA UM BAU,","VOCE QUER ABRIR?(S/N)"
840 LET O$=INKEY$
850 IF O$="" THEN GOTO 840
860 IF O$="N" THEN GOTO 360
870 LET C=INT (RND*6)+1
880 LET T=INT (RND*1000)+25
890 IF C>2 THEN GOTO 920
900 PRINT ,,"QUE PENA,EXPLODIU"
910 GOTO 940
920 PRINT ,,"VOCE ENCONTROU O OURO "
930 LET OURO=OURO+T
940 FOR L=0 TO 10
950 NEXT L
960 GOTO 360
970 REM   SALA DO TESOURO
980 IF P<15 THEN GOTO 320
990 CLS
1000 PRINT ,,"VOCE ENCONTROU A SALA DO TESOURO";
1010 PRINT "ARERTE""R"" PARA CORRER,","OU""C"" PARA CHECAR"
1020 LET R$=INKEY$
1030 IF R$<>"R" AND R$<>"C" THEN GOTO 1020
1040 IF R$="R" THEN RETURN
1050 PRINT ,,"MAS EXISTE UM GUARDA,E SE VOCE","QUER O OURO VAI TER QUE LUTAR."
1060 IF F>=50 THEN LET A=INT (RND*F)+20
1070 LET H=INT (RND*6)+1
1080 IF H=1 THEN LET N$="BRUXO"
1090 IF H=2 THEN LET N$="LOBISOMEM"
1100 IF H=3 THEN LET N$="VAMPIRO"
1110 IF H=4 THEN LET N$="ZUMBI"
1120 IF H=5 THEN LET N$="GNOMO"
1130 IF H=6 THEN LET N$="DUENDE"
1140 PRINT ,,"ELE E UM ";N$
1150 IF F<50 THEN LET A=LEN N$+INT (RND*15)+1
1160 PRINT AT 10,0;"SEUS PONTOS-->";F;"  ";AT 11,0;"PONTOS DO ";N$;"-->";A;"  "
1170 IF F<=0 THEN GOTO 1500
1180 IF A<=0 THEN GOTO 1230
1190 LET A=A-INT (RND*200)
1200 IF H=1 THEN LET F=F-INT LEN N$*(RND*LEN N$+LEN N$)
1210 IF H<>1 THEN LET F=F-INT (RND*200)
1220 GOTO 1160
1230 REM   VITORIA
1240 CLS
1250 PRINT "O OURO DOS DEMONIOS E SEU."
1260 LET OURO=OURO+INT (RND*1000)
1270 PRINT "OURO=";OURO
1280 PRINT "PONTOS=";F
1290 IF F>1200 THEN LET S$="VOCE ENCONTROU O CETRO DO PODER."
1300 IF F>1200 AND OURO>5000 THEN LET S$=S$+"VOCE TAMBEM GANHOU UMA HONRRARIA DO REI UKRACIUS"
1310 IF F<1200 AND F>600 THEN LET S$="VOCE SOBREVIVEU POR PURA SORTE"
1320 IF F<=600 THEN LET S$="A UNICA COISA QUE VOCE FEZ FOI SOBREVIVER"
1330 PRINT S$
1340 IF F>1200 THEN PRINT AT 5,0;"O CASAMENTO COM A PRINCESA SERA BREVE."
1350 IF F<1200 THEN PRINT AT 5,0;"VOCE TERA QUE SE CONTENTAR COM  UMA PLEBEIA QUALQUER."
1360 REM   NOVO JOGO
1370 PRINT ,,"JOGA NOVAMENTE?(S/N)"
1380 IF INKEY$="S" THEN RUN
1390 GOTO 1380
1400 CLS
1410 SLOW
1420 REM   VENCE O MONSTRO
1430 PRINT "VOCE MATOU O ";N$;" SEUS PONTOS ESTAO AUMENTANDO"
1440 LET F=F+INT (RND*F*2)+5
1450 LET OURO=OURO+INT (RND*300)+25
1460 FOR L=0 TO 30
1470 NEXT L
1480 GOTO 360
1490 REM   E DERROTADO
1500 CLS
1510 PRINT "O ";N$;"MATOU VOCE"
1520 PRINT "E ROUBOU SEU OURO"
1530 GOTO 1370
1540 REM   APRESENTACAO
1550 PRINT AT 0,5;"CAVERNA MALDITA";AT 1,5;"\''\''\''\''\''\''\''\''\''\''\''\''\''\''"
1560 PRINT AT 3,0;"DESEJA INSTRUCOES?(S/N)"
1570 IF INKEY$="S" THEN GOTO 1600
1580 IF INKEY$="N" THEN RETURN
1590 GOTO 1570
1600 PRINT AT 3,0;"VOCE FOI INCUMBIDO PELO REI DE  UKRACIUS PARA ENCONTRAR O CETRO DO PODER."
1610 PRINT "O CETRO FOI ROUBADO POR UMA","LEGIAO DE SERES MALEFICOS QUE   MORAM NA CAVERNA MALDITA."
1620 PRINT "CUIDADO COM OS PRESENTES QUE ","ACEITA,ELES PODEM ESTAR AMALDICOADOS."
1630 PRINT "CUIDADO COM OS BAUS,ELES PODEM  EXPLODIR MAS TAMBEM PODEM TER OURO."
1640 PRINT " RECUPERANDO 0 CETRO E PEGANDO  BASTANTE OURO, PODERA RECEBER A MAO DA PRINCESA EM CASAMENTO."
1650 PRINT TAB 7;"**BOA SORTE**"
1660 PRINT "APERTE N/L PARA JOGAR"
1670 IF INKEY$="" THEN GOTO 1670
1680 RETURN

