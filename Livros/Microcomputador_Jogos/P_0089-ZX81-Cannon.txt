'- Cannon , ZX-81 , Microcomputador Jogos , Página 89
'- status não informado pelo Digitador
'- Digitador desconhecido

10 REM                                          CANNON                   PARA LINHA SINCLAIR
20 POKE 16418,0
30 GOSUB 1270
40 REM   GERA LAY-OUT
50 FOR A=0 TO 23
60 PRINT AT A,0;"% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % "
70 NEXT A
80 LET P=50
90 LET U=6
100 FOR A=1 TO 15
110 PRINT AT A,4;"\##\##\##\##";TAB 22;"\##\##\##\##"
120 NEXT A
130 FOR A=1 TO 4
140 PRINT TAB 4;"\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##"
150 NEXT A
170 REM   CRIA VARIAVEIS
180 LET U=0
190 LET N=0
200 LET F=0
210 LET D=1
220 LET Z=1+PEEK 16396+256*PEEK 16397
230 LET O=Z+172
240 LET S=0
250 POKE O,135
260 REM   CONTROLE DOS INV
270 LET A=Z+INT (RND*13)+8
280 LET B=Z+INT (RND*13)+8
290 POKE A,128
300 LET P=P+2.5
310 LET A=A+INT (RND*3)+32
320 IF PEEK A=8 THEN GOSUB 810
330 IF PEEK A=149 THEN GO5UB 950
340 IF PEEK A=136 THEN LET A=Z+INT (RND*13)+8
350 POKE A,151
360 IF PEEK 16421<>255 THEN GOSUB 470
370 POKE B,128
380 LET B=B+INT (RND*3)+32
390 IF PEEK B=8 THEN GOSUB 810
400 IF PEEK B=149 THEN GOSUB 950
410 IF F=1 THEN GOTO 1110
420 IF PEEK B=136 THEN LET B=Z+INT (RND*13)+8
430 POKE B,151
440 IF PEEK 16421<>255 THEN GOSUB 470
450 GOTO 290
460 REM   CONTROLE DO CANHAO
470 LET O=PEEK 16421
480 PRINT AT 22,1;"\::\::\::\::\::\::\::\::\::\::\::\::\::"
490 IF P<0 THEN PRINT AT 22,1;"%S%E%M% %E%N%E%R%G%I%A"
500 LET U=O
510 IF INKEY$="7" AND PEEK (C-33)=8 THEN LET C=C-33
520 IF INKEY$="6" AND PEEK (C+33)=8 THEN LET C=C+33
530 IF INKEY$="8" AND D=1 THEN GOTO 600
540 IF INKEY$="5" AND D=-1 THEN GOTO 650
550 IF O<›V THEN POKE V,8
560 IF D=1 THEN POKE C,135
570 IF D=-1 THEN POKE C,4
580 IF INKEY$="0" AND P>0 THEN GOSUB 710
590 RETURN
600 LET D=-1
610 LET O=C+15
620 IF PEEK C=128 THEN GOTO 650
630 POKE C,8
640 GOTO 540
650 LET D=1
660 LET C=C-15
670 IF PEEK C=128 THEN GOTO 600
680 POKE O,8
690 GOTO 540
700 REM   CONTROLE DO TIRO
710 LET P=P-10
720 FOR N=C+D TO C+(D*7) STEP D
730 IF PEEK N=151 THEN GOTO 810
740 POKE N,150
750 NEXT N
760 FOR X=C+D TO N STEP D
770 POKE X,128
780 NEXT X
790 RETURN
800 REM   EXPLOSAO DO INV
810 IF PEEK N=151 THEN LET H=N
820 IF PEEK N=151 THEN LET S=S+10
830 IF PEEK A=8 THEN LET H=A
840 IF PEEK B=8 THEN LET H=B
850 FOR I=1 TO 5
860 POKE H,23
870 POKE H,151
880 NEXT I
890 POKE H,126
900 IF H=A THEN LET A=Z+INT (RND*14)+9
910 IF H=B THEN LET B=Z+INT (RND*14)+9
920 IF H=N THEN GOTO 760
930 RETURN
940 REM   EXPLOSAO DO COMBUSTIVEL
950 LET I=155
960 IF PEEK A=149 THEN LET H=A
970 IF PEEK B=149 THEN LET H=B
980 POKE H,I
990 POKE H-32,I
1000 POKE H-34,I
1010 POKE H-63,I
1020 POKE H-69,I
1030 POKE H-29,I
1040 POKE H-37,I
1050 IF 1=128 AND O=0 THEN LET F=1
1060 IF 1=128 THEN GOTO 900
1070 LET I=128
1080 LET J=J-1
1090 GOTO 980
1100 REM   PONTUACAO
1110 PRINT AT 21,1;"%V%O%C%E% %F%E%Z% "
1120 LET S$=STR$ S
1130 FOR L=1 TO LEN S$
1140 PRINT CHR$ (CODE S$(L)+128)
1150 NEXT L
1160 PRINT "% %P%O%N%T%O%S"
1170 IF S$=STR$ U THEN GOTO 1220
1180 IF S>U THEN LET U=S
1190 PRINT AT 22,1;"%O% %R%E%C%O%R%D% %E% "
1200 LET S$=STR$ U
1210 GOTO 1130
1220 IF INKEY$<>"" THEN GOTO 1220
1230 PRINT AT 9,5;"APERTE QUALQUER TECLA"
1240 PRINT AT 9,5;"%A%P%E%R%T%E% %Q%U%A%L%Q%U%E%R% %T%E%C%L%A"
1250 IF INKEY$="" THEN GOTO 1230
1260 GOTO 20
1270 REM   APRESENTACAO
1280 PRINT AT 10,2;"DESEJA INSTRUCOES?(S/N)"
1290 IF INKEY$="S" THEN GOTO 1320
1300 IF INKEY$="N" THEN RETURN
1310 GOTO 1290
1320 CLS
1321 PRINT TAB 11;"CANNON",TAB 11;"\''\''\''\''\''\''"
1330 PRINT "VOCE TERA OUE DEFENDER A BASE","DE COMBUSTIVEL SITUADA NO FUNDO","DE UM VALE SECRETO.","O SEU CANHAO PODE FICAR EM","EM OUALOUER UMA DAS ENCOSTAS ","DO PROFUNDO VALE."
1340 PRINT "SEUS INIMIGOS JOGARAO MISSEIS","DO ALTO DAS ENCOSTAS, E A SUA","MISSA0 E DESTRUILOS COM","O CANHAO PULSAR."
1350 PRINT "SE UM DOS MISSEIS BATER NAS ","ENCOSTAS DO VALE,O UNICO JEITO","DE VOCE SUBIR OU DESCER,","E PELO OUTRO LADO."
1360 PRINT "%7-->SOBE",,"%6-->DESCE",,"%8-->ENCOSTA DIREITA","%5-->ENCOSTA ESQUERDA"
1370 PRINT AT 20,2;"PRESSIONE N/L PARA JOGAR"
1380 IF INKEY$="" THEN GOTO 1380
1390 RETURN

