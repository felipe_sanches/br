'- Come Come , ZX-81 , Microcomputador Jogos , Página 15
'- status não informado pelo Digitador
'- Digitador desconhecido

1 REM *********************************
2 REM ***** PARA A LINHA SINCLAIR *****
3 REM ********C O M E - C O M E *******
4 REM *********************************
5 REM *** O LABIRINTO ***
10 FAST
20 PRINT
25 REM *** CONSTRUCAO DAS PAREDES SEM SAIDA ***
30 FOR I=1 TO 10
40 PRINT "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"
50 PRINT "H............................H"
60 NEXT I
70 PRINT "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"
80 FOR N= 1 TO 30 STEP 14
90 FOR J= 7 TO 19 STEP 4
100 PRINT AT J,N;"."
110 NEXT J
120 NEXT N
125 REM *** DEFINICAO DOS CAMINHOS DENTRO DO LABIRINTO ***
130 FOR N=5 TO 17 STEP 6
140 PRINT AT N,11;".";AT N,21;"."
150 NEXT N
160 PRINT AT 9,2;".";AT 9,30;"."
170 PRINT AT 13,2;".";AT 13,30;"."
180 PRINT AT 7,15;".";AT 7,17;"."
190 PRINT AT 15,15;".";AT 15,17;"."
200 PRINT AT 9,16;".";AT 13,16;"."
210 PRINT AT 11,3;".";AT 11,29;"."
220 PRINT AT 6,16;"H";AT 7,16;"H"
230 PRINT AT 11,16;"H";AT 15,16;"H"
240 PRINT AT 16,16;"H"
250 FOR N=1 TO 13 STEP 2
260 PRINT AT N,4;"."
270 PRINT AT N,28;"."
280 NEXT N
290 FOR N=8 TO 14 STEP 2
300 PRINT AT N,3;"H"
310 PRINT AT N,29;"H"
320 NEXT N
325 REM *** A ENTRADA NO LABIRINTO ***
330 PRINT AT 0,0;" "
340 LET C=PEEK 16398+PEEK 16399*255+115
350 LET P=C+495
360 LET S=27
370 SLOW
380 REM *** 0 JOGO ***
390 LET B=6.5
400 LET X=0
405 REM *** AGUARDA ENTRADA DO TECLADO ***
410 LET M$=INKEY$
420 IF M$<>"" THEN LET B=VAL M$
430 POKE C,S
440 POKE P,0
450 LET X=34*B-50*SGN(B-6.5)-221
460 IF PEEK (P+X)<>136 THEN LET P=P+X
465 REM *** MONTA O MONSTRO ***
470 POKE P,128
480 LET Y=33*SGN (INT ((P-C)/28))
490 IF PEEK (C+Y)<> 136 THEN LET C=C+Y
500 LET Y=SGN ((ABS ((P-C)/33-INT ((P-C)/33))<.5)-.5)
510 IF PEEK (C+Y)<>136 THEN LET C=C+Y
520 LET S=PEEK C
525 REM *** 0 MONSTRO PERSEGUINDO ***
530 POKE C,151
535 REM *** VERIFICA SE 0 MONSTRO TE PEGOU ***
540 IF P=C THEN GOTO 560
550 GOTO 380
560 REM *** FINAL ***
570 CLS
580 PRINT AT 10,10;" TENTE DE NOVO, PORQUE DESTA VEZ "
590 PRINT
600 FOR N=1 TO 10
610 PRINT "       "
620 PRINT "NAO DEU"
630 NEXT N
640 PRINT "QUER TENTAR DE NOVO? S/N"
650 INPUT B$
660 IF B$="S" THEN GOTO 1
670 PRINT
680 PRINT
690 PRINT"TCHAU"

