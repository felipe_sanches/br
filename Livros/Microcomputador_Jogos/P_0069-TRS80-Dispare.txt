'- Dispare , TRS-80 , Microcomputador Jogos , Página 69
'- status não informado pelo Digitador
'- Digitador desconhecido

100 REM *********************************
110 REM *        PARA LINHA TRS-80      *
120 REM *           DISPARE:            *
130 REM *********************************
140 REM
150 REM ***   TELAS DE 1NSTRUCOES     ***
160 CLS:CLEAR 6000:PRINT@ 21,"<<  DISPARE !  >>"
170 PRINT:PRINT:PRINT TAB(5) "O objetivo de <DISPARE> e' abater o maior numero de ":PRINT TAB(5) "naves inimigas com   misseis teleguiaveis."
180 PRINT TAB(5) "Voce so' dispoe de 15 misseis para abater o inimigo.": PRINT TAB(5) "A base dos misseis fica no centro inferior da tela.": PRINT TAB(5) "'A esquerda, voce ve numero de naves abatidas."
184 PRINTTAB(17) "USE QQ TECLA PARA CONTINUAR"
185 I$="":I$=INKEYS:IFI$=""THEN185
186 CLS
190 PRINT TA8(5) "'A direita fica o display do numero de pontos ganhos.": PRINT TAB(5) "Sempre existirao 4 naves na tela. Elas se movimentam": PRINT TAB(5) "a uma velocidade que vai de 1 a 5. Os pontos ganhos";
200 PRINT TAB(5) "por cada nave abatida sao exatamente a sua velocidade,": PRINT TAB(5) "portanto procure acertar as naves mais rapidas."
210 PRINT:PRINT:PRINT TAB(17) "USE Q0 TECLA PARA CONTINUAR";
220 I$="":I$=INKEY$:IF I$="" THEN 220
230 CLS
240 PRINT TABt5) "Os misseis sao disparados atraves das teclas:": PRINT TAB(10) "Barra de Espacos - Disparo na Vertical"
250 PRINT TAB(10) "Seta <- ou Z - Disparo diagonal p/ Esquerda": PRINT TAB(10) "Seta -> ou / - Disparo diagonal p/ Direita"
260 PRINT TAB(5) "Voce controlara' o rumo do missil atraves dessas": PRINT TAB(5) "teclas tambem. Se voce for MUITO bom, podera' abater": PRINT TAB(5) "todos os inimigos de uma so' vez..."
264 PRINTTAB(17)"USE QQ TECLA PARA CONTINUAR"
265 I$="":I$=INKEY$:IF I$=""THEN265
269 CLS
270 PRINT TAB(5) "Quando terminar o jogo, serao mostrados o numero": PRINT TAB(5) "de pontos, o melhor placar e o numero de naves abatidas."
280 PRINT:PR1NT TAB(19) "USE QQ TECLA PARA JOGAR";
290 I$="":I$=INKEY$:IF I$="" THEN 290
300 GOSUB 870
310 REM ***    DESENHOS DOS OBJETOS   ***
320 X$=CHR$(149)+" "+CHR$(170)
330 A$(0)=CHR$(166)+CHR$(183)+CHR$(183)+ CHR$(183)+CHR$(132)
340 A$(1)=CHR$(174)+CHR$(187)+CHR$(187)+ CHR$(187)+CHR$(132)
350 B$(0)=CHR$(152)+CHR$(179)+CHR$(143)+ CHR$(179)+CHR$(164)
360 B$(1)=CHR$(168)+CHR$(163)+CHR$(143)+ CHR$(147)+CHR$(148)
370 C$(0)=CHR$(176)+CHR$(143)+CHR$(131)+ CHR$(143)+CHR$(176)
380 C$(1)-CHR$(140)+CHR$(179)+CHR$(131)+ CHR$(179)+CHR$(140)
390 D$(0)=CHR$(156)+CHR$(174)+CHR$(179)+ CHR$(157)+CHR$(172)
400 D$(1)=CHR$(141)+CHR$(174)+CHR$(179)+ CHR$(157)+CHR$(142)
410 REM ***       INICIO DO JOGO       ***
420 CLS:D$=STRING$(15,131):GOSUB 740  : GOSUB 750 :GOSUB 760 :GOSUB 770  : PRINT@ 993,D$;:F=0:S=990:F9=1
430 PRINT@ 0,;
440 REM ***      VERIFICA TECLADO      ***
450 IF PEEK(14656)=32 OR PEEK(15119)=4 THEN F=1
460 IF PEEK(14656)=64 OR PEEK(14880)=128 THEN F=2
470 IF PEEK(14912)=128 THEN F=3
480 REM ***    TRAJETORIA DO MISSIL    ***
490 PRINT@ S,".";
500 IF F=1 THEN S=S-67 ELSE IF F=2 THEN S=S-61 ELSE IF F=3 THEN S=S-64
510 PRINT@ 989,X$;
520 REM ***     MISSIL SAI DA TELA     ***
530 IF S<0 THEN FOR X=9000 TO 9008: L=USR(X):NEXT:CLS:F=0:S=990:D$= LEFT$(D$,LEN(D$)-1):IF LEN(D$)=0 THEN 790 ELSE PRINT@ 993, D$;
540 REM ***        ATUALIZA TELA       ***
550 PRINT@ S,CHR$(131);:PRINT@ 1009, "PLACAR:";SC;:IF S<>990 THEN PRINT@ 0,;:X=USR(9000)
560 PRINT@ 960,"NAVES ATINGIDAS:";SH;
570 F9=1-F9
580 REM ***        ATINGIU NAVE        ***
590 IF S>A1-3 AND S<A1+5 THEN PRINT@ A1, "B U M!";:PRINT@ 0,;:X=USR(40):PRINT@ A1,"      ";:SC=SC+B1:SH=SH+1: GOSUB 740
600 IF S>A2-3 AND S<A2+5 THEN PRINT@ A2, "B U M!";:PRINT@ 0,;:X=USR(45):PRINT@ A2,"      ";:SC=SC+B2:SH=SH+1: GOSUB 750
610 IF S>A3-3 AND S<A3+5 THEN PRINT@ A3, "B U M!";:PRINT@ 0,;:X=USR(50):PRINT@ A3,"      ";:SC=SC+B3:SH=SH+1: GOSUB 760
620 IF S>A4-3 AND S<A4+5 THEN PRINT@ A4, "B U M!"::PRINT@ 0,;:X=USR(55):PRINT@ A4,"      ";:SC=SC+B4:SH=SH+1: GOSUB 770
630 REM ***       MOVIMENTA NAVES      ***
640 PRINT@ A1,"      ";:A1=A1+B1:PRINT@ A1,A$(F9);
650 PRINT@ A2,"      ";:A2=A2-B2:PRINT@ A2,B$(F9);
660 PRINT@ A3,"      ";:A3=A3+B3:PRINT@ A3,C$(F9);
670 PRINT@ A4,"      ";:A4=A4+B4:PRINT@ A4,D$(F9);
680 IF A1>62 THEN PRINT@ Al,"      ";: GOSUB 740
690 IF A2<128 THEN PRINT@ A2,"      ";: GOSUB 750
700 IF A3>318 THEN PRINT@ A3,"      ";: GOSUB 760
710 IF A4>446 THEN PRINT@ A4,"      ";: GOSUB 770
720 GOTO 430
730 REM ***     VELOCIDADE DAS NAVES   ***
740 A1=0:B1=RND(5):RETURN
750 A2=191:B2=RND(5):RETURN
760 A3=256:B3=RND(5):RETURN
770 A4=384:B4=RND(5):RETURN
780 REM ***       FINAL DE JOGADA      ***
790 PRINT@ 152,"SEM MUNICAO !!";
800 IF SC>HI THEN HI=SC
810 PRINT@ 408,"PLACAR:";SC;
820 PRINT@ 472,"MELHOR PLACAR:";HI;
830 PRINT@ 536,"ABATIDOS:";SH
840 PRINT@ 792,"JOGA DE NOVO? (S/N)"
850 I$=INKEY$:IF I$<>"S" AND I$<>"N" THEN 850
860 IF I$="S" THEN SC=0:SH=0:GOTO 420 ELSE CLS:END
870 REM ***   INTRODUZ ROTINA DE SOM   ***
880 S$="":FOR GH=1 TO 23:READ GJ:S$=S$+ CHR$(GJ):NEXT GH
890 DATA 205,127,10,69,62,1,211,255,16,254,69,60,211,255,16,254,37,32,240,175,211,255,201
900 V=VARPTR(S$): J=PEEK(V+1)+256*PEEK(V+2)
910 IF PEEK(16396)=201 THEN POKE 16526,PEEK(V+1):POKE 16527,PEEK(V+2) ELSE DEFUSR=J-65536:RETURN
920 FOR X%=1 TO 15:L=USR(X%):NEXT
930 RETURN

